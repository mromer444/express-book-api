const express = require('express');
const router = express.Router();
const Joi = require('joi');



const books = [
    {id:1 , name: 'book1'},
    {id:2 , name: 'book2'},
    {id:3 , name: 'book3'},
    {id:4 , name: 'book4'},
]




//get all books
router.get('/' , (req , res) => {
    res.send(books);
});


//get book by ID
router.get('/:id' , (req , res) => {
    const book = books.find(current_book => current_book.id === parseInt(req.params.id));
    if(!book){
        res.status(404).send(`there is no book with this ID ${req.params.id} `);
    }else{
        res.send(book);
    }
});


//create book
router.post('/' , (req , res) => {
    const { error } = validation(req.body);
    if(error){
        res.status(404).send(error.details[0].message);
        return;
    };

    const book = {
        id: books.length+1,
        name: req.body.name
    };

    books.push(book);
    res.send(book);
});




//update book
router.put('/:id' , (req , res) => {
    const book = books.find(current_book => current_book.id === parseInt(req.params.id));
    if(!book){
        res.status(`there is no book with this id ${req.params.id}`);
        return;
    }else{
        const { error } = validation(req.body);
        if(error){
            res.status(404).send(error.details[0].message);
            return;
        }
        book.name = req.body.name;
        res.send(book);
    }
});




router.delete('/:id' , (req , res) => {
    const book = books.find(current_book => current_book.id === parseInt(req.params.id));
    if(!book){
        res.status(404).send(`there is no book with this id ${req.params.id}`);
        return;
    }else{
        const index_of_book = books.indexOf(book);
        books.splice(index_of_book , 1);
        res.send(book);
    }
});






function validation(book){
    const schema = {
        name: Joi.string().min(3).max(10).required()
    };
    return Joi.validate(book , schema);
};










module.exports = router;